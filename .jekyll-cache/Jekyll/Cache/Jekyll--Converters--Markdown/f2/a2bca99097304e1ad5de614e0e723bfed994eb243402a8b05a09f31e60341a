I";<p>Through the last few years, I’ve mentored a few folks at Exercism.</p>

<p>Once thing tickles me everytime is how we (as individual) tends to write software.</p>

<p>Often, imperative approach is used to describe what we want to achieve. It’s not a bad thing, not at all. You sometimes need to be imperative.</p>

<p>Nevertheless, as a software engineer, I like to use declarative approach. Since we’re writing code for people and not machine. I think it’s the best I can do to communicate my intent the better way.</p>

<!--more-->

<hr />

<h2 id="leetcode">Leetcode</h2>

<h3 id="the-problem">The problem</h3>

<p>Given a 0-indexed integer array nums of size <code class="language-plaintext highlighter-rouge">n</code>, find the maximum difference between <code class="language-plaintext highlighter-rouge">nums[i]</code> and <code class="language-plaintext highlighter-rouge">nums[j]</code> (i.e., <code class="language-plaintext highlighter-rouge">nums[j] - nums[i]</code>), such that <code class="language-plaintext highlighter-rouge">0 &lt;= i &lt; j &lt; n</code> and <code class="language-plaintext highlighter-rouge">nums[i] &lt; nums[j]</code>.</p>

<p>Return the maximum difference. If no such i and j exists, return <code class="language-plaintext highlighter-rouge">-1</code>.</p>

<h3 id="examples">Examples</h3>

<p>First</p>
<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>Input: nums = [7,1,5,4]
Output: 4
Explanation:
The maximum difference occurs with i = 1 and j = 2, nums[j] - nums[i] = 5 - 1 = 4.
Note that with i = 1 and j = 0, the difference nums[j] - nums[i] = 7 - 1 = 6, but i &gt; j, so it is not valid.
</code></pre></div></div>

<p>Second</p>
<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>Input: nums = [9,4,3,2]
Output: -1
Explanation:
There is no i and j such that i &lt; j and nums[i] &lt; nums[j].
</code></pre></div></div>

<p>Last</p>
<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>Input: nums = [1,5,2,10]
Output: 9
Explanation:
The maximum difference occurs with i = 0 and j = 3, nums[j] - nums[i] = 10 - 1 = 9.
</code></pre></div></div>

<h3 id="the-code">The code</h3>

<div class="language-elixir highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">def</span> <span class="n">maximum_difference</span><span class="p">(</span><span class="n">nums</span><span class="p">)</span> <span class="k">do</span>
    <span class="n">nums</span>
    <span class="o">|&gt;</span> <span class="no">Stream</span><span class="o">.</span><span class="n">scan</span><span class="p">(</span><span class="o">&amp;</span><span class="no">Kernel</span><span class="o">.</span><span class="n">min</span><span class="o">/</span><span class="mi">2</span><span class="p">)</span>
    <span class="o">|&gt;</span> <span class="no">Stream</span><span class="o">.</span><span class="n">zip</span><span class="p">(</span><span class="n">nums</span><span class="p">)</span>
    <span class="o">|&gt;</span> <span class="no">Stream</span><span class="o">.</span><span class="n">map</span><span class="p">(</span><span class="k">fn</span> <span class="p">{</span><span class="n">i</span><span class="p">,</span> <span class="n">j</span><span class="p">}</span> <span class="o">-&gt;</span> <span class="n">j</span> <span class="o">-</span> <span class="n">i</span> <span class="k">end</span><span class="p">)</span>
    <span class="o">|&gt;</span> <span class="no">Stream</span><span class="o">.</span><span class="n">filter</span><span class="p">(</span><span class="o">&amp;</span> <span class="nv">&amp;1</span> <span class="o">!=</span> <span class="mi">0</span><span class="p">)</span>
    <span class="o">|&gt;</span> <span class="no">Enum</span><span class="o">.</span><span class="n">max</span><span class="p">(</span><span class="o">&amp;&gt;=/</span><span class="mi">2</span><span class="p">,</span> <span class="k">fn</span> <span class="o">-&gt;</span> <span class="o">-</span><span class="mi">1</span> <span class="k">end</span><span class="p">)</span>
<span class="k">end</span>
</code></pre></div></div>

<h3 id="the-logic">The logic</h3>

<p>The first line is what we call a scan, more precisely, a min scan.</p>

<p>We’re scanning the list and applying <code class="language-plaintext highlighter-rouge">&amp;Kernel.min/2</code>.</p>

<p><em>Just a minute… What is a scan?</em></p>

<p>Refering to the docs, a scan is the following:</p>

<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>Applies the given function to each element in the enumerable, storing the result in a list and passing it as the accumulator for the next computation. Uses the first element in the enumerable as the starting value.
</code></pre></div></div>
:ET