I"fT<p>In my spare time, I like to mentor on the Exercism platform.</p>

<p>I do Elixir, Swift and a bit of Erlang, even if the latest is not a language I do quite often.</p>

<p>The Ruby syllabus is good, it covers 16 topics, from <code class="language-plaintext highlighter-rouge">Strings</code> to <code class="language-plaintext highlighter-rouge">Enumeration</code> and <code class="language-plaintext highlighter-rouge">Ostruct</code>.</p>

<p>I’d like to emphasize one exercise I’ve gone through and show how we can go from a one liner to a more production ready code.</p>

<p>The main goal is to show that, yes oneliners are cool and you can brag… but no one cares. Production code is the goto whenever you’re trying to solve a kata or whatever.</p>

<hr />

<h2 id="the-gist">The gist</h2>
<div class="language-plaintext highlighter-rouge"><div class="highlight"><pre class="highlight"><code>Given a number, find the sum of all the unique multiples of particular numbers up to but not including that number.

If we list all the natural numbers below 20 that are multiples of 3 or 5, we get 3, 5, 6, 9, 10, 12, 15, and 18.

The sum of these multiples is 78.
</code></pre></div></div>

<p>And here you can find my very first shot.</p>

<div class="language-ruby highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">class</span> <span class="nc">SumOfMultiples</span>
  <span class="k">def</span> <span class="nf">initialize</span><span class="p">(</span><span class="o">*</span><span class="n">multiples</span><span class="p">)</span>
    <span class="vi">@multiples</span> <span class="o">=</span> <span class="n">multiples</span>
  <span class="k">end</span>

  <span class="k">def</span> <span class="nf">to</span><span class="p">(</span><span class="n">limit</span><span class="p">)</span>
    <span class="k">return</span> <span class="mi">0</span> <span class="k">if</span> <span class="vi">@multiples</span><span class="p">.</span><span class="nf">include?</span><span class="p">(</span><span class="mi">0</span><span class="p">)</span>
    <span class="p">(</span><span class="mi">0</span><span class="o">...</span><span class="n">limit</span><span class="p">).</span><span class="nf">filter</span> <span class="p">{</span> <span class="o">|</span><span class="n">integer</span><span class="o">|</span> <span class="vi">@multiples</span><span class="p">.</span><span class="nf">any?</span> <span class="p">{</span> <span class="o">|</span><span class="n">multiple</span><span class="o">|</span> <span class="n">integer</span><span class="p">.</span><span class="nf">remainder</span><span class="p">(</span><span class="n">multiple</span><span class="p">).</span><span class="nf">zero?</span> <span class="p">}</span> <span class="p">}.</span><span class="nf">sum</span>
  <span class="k">end</span>
<span class="k">end</span>
</code></pre></div></div>

<p>It is quite simple, we make an early return if <code class="language-plaintext highlighter-rouge">multiples</code> contains 0.
Then we filter on the range from 0 to the <code class="language-plaintext highlighter-rouge">limit</code>, we use the block to look for a factor, the we filter and then we sum this up.</p>

<!--more-->
<hr />
<h2 id="sweet-a-one-liner">Sweet a one liner…</h2>
<hr />

<p>That’s all folks, it ends here. See you on a next one!</p>

<p>Let’s put the glasses up, crack the fingers, and let’s dig a bit.</p>

<hr />

<p>Well, at some point we recoginze that we are asking if something is a factor. So defining a <code class="language-plaintext highlighter-rouge">factor?</code> method would be helpful in communicating clearly what we want to do.</p>

<p>Let’s implement that.</p>

<div class="language-ruby highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">class</span> <span class="nc">SumOfMultiples</span>
  <span class="nb">attr_accessor</span> <span class="ss">:multiples</span>
  <span class="k">def</span> <span class="nf">initialize</span><span class="p">(</span><span class="o">*</span><span class="n">factors</span><span class="p">)</span>
    <span class="vi">@multiples</span> <span class="o">=</span> <span class="n">factors</span>
  <span class="k">end</span>

  <span class="k">def</span> <span class="nf">to</span><span class="p">(</span><span class="n">limit</span><span class="p">)</span>
    <span class="k">return</span> <span class="mi">0</span> <span class="k">if</span> <span class="n">multiples</span><span class="p">.</span><span class="nf">include?</span><span class="p">(</span><span class="mi">0</span><span class="p">)</span>
    <span class="p">(</span><span class="mi">0</span><span class="o">...</span><span class="n">limit</span><span class="p">).</span><span class="nf">sum</span> <span class="p">{</span> <span class="o">|</span><span class="n">integer</span><span class="o">|</span> <span class="n">multiples</span><span class="p">.</span><span class="nf">any?</span> <span class="p">{</span> <span class="o">|</span><span class="n">multiple</span><span class="o">|</span> <span class="n">factor?</span><span class="p">(</span><span class="n">integer</span><span class="p">,</span> <span class="n">multiple</span><span class="p">)</span> <span class="p">}</span> <span class="p">?</span> <span class="n">integer</span> <span class="p">:</span> <span class="mi">0</span> <span class="p">}</span>
  <span class="k">end</span>

  <span class="kp">private</span> <span class="k">def</span> <span class="nf">factor?</span><span class="p">(</span><span class="n">integer</span><span class="p">,</span> <span class="n">multiple</span><span class="p">)</span>
    <span class="n">integer</span><span class="p">.</span><span class="nf">remainder</span><span class="p">(</span><span class="n">multiple</span><span class="p">).</span><span class="nf">zero?</span>
  <span class="k">end</span>
<span class="k">end</span>
</code></pre></div></div>

<p>The <code class="language-plaintext highlighter-rouge">factor?</code> follows the idiom of def that ends with a <code class="language-plaintext highlighter-rouge">?</code> and returns a boolean.</p>

<p>We check if the remainder of the division of <code class="language-plaintext highlighter-rouge">integer</code> by <code class="language-plaintext highlighter-rouge">multiple</code> is equal to zero.</p>

<p>It simplify a bit the <code class="language-plaintext highlighter-rouge">to(limit)</code> function, furthermore, the <code class="language-plaintext highlighter-rouge">sum</code> takes a block so we can do some stuff inside it.</p>

<p>One modification done is changing how we acces instance variable by leveraging attribute methods, <a href="https://ivoanjo.me/blog/2017/09/20/why-i-always-use-attr_reader-to-access-instance-variables/">you can read more here</a></p>

<p>Inside the block, we access the <code class="language-plaintext highlighter-rouge">multiples</code> instance variable and do a little ternary, if we’ve a factor, return the <code class="language-plaintext highlighter-rouge">multiple</code> otherwise 0.</p>

<p>In short, this first iteration enhance the intent and makes the code much easier to read, despite not being a oneliner anymore… rip.</p>

<hr />

<p>Still I was not really conviced it was over. After all, we can see a pattern emerging. Do you see it?</p>

<ul>
  <li>public method</li>
  <li>private mehtod</li>
  <li>public method</li>
  <li>private mehtod</li>
</ul>

<p>What a strange style. It may lead to some cargo cultisme. And yes, <a href="http://jackxxu.github.io/ruby/2016/06/02/ruby-private-initialize-method/">initialize is private</a>.</p>

<p>Let’s tackle this.</p>

<hr />

<div class="language-ruby highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">class</span> <span class="nc">SumOfMultiples</span>

  <span class="kp">private</span>

  <span class="k">def</span> <span class="nf">initialize</span><span class="p">(</span><span class="o">*</span><span class="n">factors</span><span class="p">)</span>
    <span class="vi">@multiples</span> <span class="o">=</span> <span class="n">factors</span>
  <span class="k">end</span>

  <span class="k">def</span> <span class="nf">factor?</span><span class="p">(</span><span class="n">integer</span><span class="p">)</span>
    <span class="n">multiples</span><span class="p">.</span><span class="nf">any?</span> <span class="p">{</span> <span class="o">|</span><span class="n">multiple</span><span class="o">|</span> <span class="p">(</span><span class="n">integer</span> <span class="o">%</span> <span class="n">multiple</span><span class="p">).</span><span class="nf">zero?</span> <span class="p">}</span> <span class="p">?</span> <span class="n">integer</span> <span class="p">:</span> <span class="mi">0</span>
  <span class="k">end</span>

  <span class="kp">public</span>

  <span class="nb">attr_accessor</span> <span class="ss">:multiples</span>

  <span class="k">def</span> <span class="nf">to</span><span class="p">(</span><span class="n">limit</span><span class="p">)</span>
    <span class="k">return</span> <span class="mi">0</span> <span class="k">if</span> <span class="n">multiples</span><span class="p">.</span><span class="nf">include?</span><span class="p">(</span><span class="mi">0</span><span class="p">)</span>
    <span class="p">(</span><span class="mi">0</span><span class="o">...</span><span class="n">limit</span><span class="p">).</span><span class="nf">sum</span> <span class="p">{</span> <span class="o">|</span><span class="n">integer</span><span class="o">|</span> <span class="n">factor?</span><span class="p">(</span><span class="n">integer</span><span class="p">)</span> <span class="p">}</span>
  <span class="k">end</span>
<span class="k">end</span>
</code></pre></div></div>

<p>So I’ve decided to make it the more expressive possible. Another enhancement is to use <code class="language-plaintext highlighter-rouge">%</code> rather than remainder.</p>

<p>Furthermore, if we think about it twice, it’s the <code class="language-plaintext highlighter-rouge">factor</code>’s duty to return either an integer or 0 if it’s not a multiple.</p>

<h3 id="huho">Huho…</h3>

<p>Why on earth would I put the <code class="language-plaintext highlighter-rouge">attr_accessor</code> inside the public area, it should not be accessible from the outside. Moreover, <code class="language-plaintext highlighter-rouge">multiples=</code> is never used. It’s a vulnerability that we need to cover.</p>

<div class="language-ruby highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">class</span> <span class="nc">SumOfMultiples</span>

  <span class="kp">private</span>

  <span class="nb">attr_reader</span> <span class="ss">:multiples</span>

  <span class="k">def</span> <span class="nf">initialize</span><span class="p">(</span><span class="o">*</span><span class="n">factors</span><span class="p">)</span>
    <span class="vi">@multiples</span> <span class="o">=</span> <span class="n">factors</span>
  <span class="k">end</span>

  <span class="k">def</span> <span class="nf">factor?</span><span class="p">(</span><span class="n">integer</span><span class="p">)</span>
    <span class="n">multiples</span><span class="p">.</span><span class="nf">any?</span> <span class="p">{</span> <span class="o">|</span><span class="n">multiple</span><span class="o">|</span> <span class="p">(</span><span class="n">integer</span> <span class="o">%</span> <span class="n">multiple</span><span class="p">).</span><span class="nf">zero?</span> <span class="p">}</span> <span class="p">?</span> <span class="n">integer</span> <span class="p">:</span> <span class="mi">0</span>
  <span class="k">end</span>

  <span class="kp">public</span>

  <span class="k">def</span> <span class="nf">to</span><span class="p">(</span><span class="n">limit</span><span class="p">)</span>
    <span class="k">return</span> <span class="mi">0</span> <span class="k">if</span> <span class="n">multiples</span><span class="p">.</span><span class="nf">include?</span><span class="p">(</span><span class="mi">0</span><span class="p">)</span>
    <span class="p">(</span><span class="mi">0</span><span class="o">...</span><span class="n">limit</span><span class="p">).</span><span class="nf">sum</span> <span class="p">{</span> <span class="o">|</span><span class="n">integer</span><span class="o">|</span> <span class="n">factor?</span><span class="p">(</span><span class="n">integer</span><span class="p">)</span> <span class="p">}</span>
  <span class="k">end</span>
<span class="k">end</span>
</code></pre></div></div>

<p>Nice. It’s clean and tidy, I mean really?</p>

<hr />

<p>Is <code class="language-plaintext highlighter-rouge">factor?</code> really checking for a factor? It should be put inside another function!</p>

<div class="language-ruby highlighter-rouge"><div class="highlight"><pre class="highlight"><code>  <span class="k">def</span> <span class="nf">factor?</span><span class="p">(</span><span class="n">integer</span><span class="p">,</span> <span class="n">multiple</span><span class="p">)</span> <span class="o">=</span> <span class="p">(</span><span class="n">integer</span> <span class="o">%</span> <span class="n">multiple</span><span class="p">).</span><span class="nf">zero?</span>

  <span class="k">def</span> <span class="nf">has_factor?</span><span class="p">(</span><span class="n">integer</span><span class="p">)</span>
    <span class="n">multiples</span><span class="p">.</span><span class="nf">any?</span> <span class="p">{</span> <span class="o">|</span><span class="n">multiple</span><span class="o">|</span> <span class="n">factor?</span><span class="p">(</span><span class="n">integer</span><span class="p">,</span> <span class="n">multiple</span><span class="p">)</span> <span class="p">}</span> <span class="p">?</span> <span class="n">integer</span> <span class="p">:</span> <span class="mi">0</span>
  <span class="k">end</span>
</code></pre></div></div>

<hr />

<p>Et voila!</p>

<p>Well, I’m not really a fan of this. <code class="language-plaintext highlighter-rouge">factor?</code> is quite bugging me.</p>

<p>So the factor? method has room to explore refinements. It would allow multiple.factor?(integer) where as a human we would read it as is multiple a factor of integer.</p>

<p>Indeed, there are the two ways to express this, the name of the method will or at least should encourage one way or the other. is integer a factor of multiple? or is multiple a factor of integer? One way it might be, the other way may never be.</p>

<p>English is strange in a way, because I could read it either way with the given names and still have to think about what it is saying. Perhaps factor_of? would clarify the direction?</p>

<p>I’d be happier if I could do <code class="language-plaintext highlighter-rouge">multiple.factor?(integer)</code>, it’d be terrific.</p>

<h3 id="refinement-or-monkey-patching">Refinement or Monkey patching?</h3>

<p>Ruby allows you to extend the language. It’s not an uncommon thing in programming. In Swift, you’ve got the <code class="language-plaintext highlighter-rouge">extend</code> keyword. It lets you add some functionnality to existing classes or structs.</p>

<p>In Ruby, there’s somehow to flavors:</p>

<ul>
  <li>Monkey patching</li>
  <li>Refinement</li>
</ul>

<p>Monkey patching is global, so in the scope of this exercise, there’s no biggie. But if we were only limited to the scope of this exercise, this article wouldn’t have any reasons to exists. Or maybe someone will use this idea in his billion dollar app…</p>

<p>How do they  work then?</p>

<p>This way.</p>

<div class="language-ruby highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">class</span> <span class="nc">Integer</span>
  <span class="k">def</span> <span class="nf">factor_of?</span><span class="p">(</span><span class="n">integer</span><span class="p">)</span>
    <span class="p">(</span><span class="n">integer</span> <span class="o">%</span> <span class="nb">self</span><span class="p">).</span><span class="nf">zero?</span>
  <span class="k">end</span>
<span class="k">end</span>
</code></pre></div></div>

<p>I directly modify the <code class="language-plaintext highlighter-rouge">Integer</code> class and add a <code class="language-plaintext highlighter-rouge">factor_of?</code> method. Sweet. But what if it clashes with other methods defined elsewhere? Oh that’s a nice mess you have here.</p>

<p>Let’s check what refinement is.</p>

<div class="language-ruby highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">module</span> <span class="nn">MathRefinements</span>
  <span class="n">refine</span> <span class="no">Integer</span> <span class="k">do</span>
    <span class="k">def</span> <span class="nf">factor_of?</span><span class="p">(</span><span class="n">integer</span><span class="p">)</span>
      <span class="p">(</span><span class="n">integer</span> <span class="o">%</span> <span class="nb">self</span><span class="p">).</span><span class="nf">zero?</span>
    <span class="k">end</span>
  <span class="k">end</span>
<span class="k">end</span>
</code></pre></div></div>

<p>Here we define a module called <code class="language-plaintext highlighter-rouge">MathRefinements</code>, inside we refine the Integer class and add the <code class="language-plaintext highlighter-rouge">factor_of?</code> mehtod.
Unlike monkey patching, refinements are not global. Indeed, hence it’s a module, we need to ask for it.</p>

<p>Let’s do this with other few enhancement.</p>

<div class="language-ruby highlighter-rouge"><div class="highlight"><pre class="highlight"><code><span class="k">module</span> <span class="nn">MathRefinements</span>
  <span class="n">refine</span> <span class="no">Integer</span> <span class="k">do</span>
    <span class="k">def</span> <span class="nf">factor_of?</span><span class="p">(</span><span class="n">integer</span><span class="p">)</span>
      <span class="p">(</span><span class="n">integer</span> <span class="o">%</span> <span class="nb">self</span><span class="p">).</span><span class="nf">zero?</span>
    <span class="k">end</span>
  <span class="k">end</span>
<span class="k">end</span>

<span class="c1"># With Monkey Patch</span>
<span class="c1"># class Integer</span>
<span class="c1">#   def factor_of?(integer)</span>
<span class="c1">#     (integer % self).zero?</span>
<span class="c1">#   end</span>
<span class="c1"># end</span>

<span class="k">class</span> <span class="nc">SumOfMultiples</span>

  <span class="n">using</span> <span class="no">MathRefinements</span>

  <span class="kp">private</span>

  <span class="nb">attr_reader</span> <span class="ss">:multiples</span>

  <span class="k">def</span> <span class="nf">initialize</span><span class="p">(</span><span class="o">*</span><span class="n">factors</span><span class="p">)</span>
    <span class="vi">@multiples</span> <span class="o">=</span> <span class="n">factors</span>
  <span class="k">end</span>

  <span class="k">def</span> <span class="nf">factors_of</span><span class="p">(</span><span class="n">integer</span><span class="p">)</span>
    <span class="n">multiples</span><span class="p">.</span><span class="nf">any?</span> <span class="p">{</span> <span class="o">|</span><span class="n">multiple</span><span class="o">|</span> <span class="n">multiple</span><span class="p">.</span><span class="nf">factor_of?</span><span class="p">(</span><span class="n">integer</span><span class="p">)</span> <span class="p">}</span> <span class="p">?</span> <span class="n">integer</span> <span class="p">:</span> <span class="mi">0</span>
  <span class="k">end</span>

  <span class="kp">public</span>

  <span class="k">def</span> <span class="nf">to</span><span class="p">(</span><span class="n">limit</span><span class="p">)</span>
    <span class="k">return</span> <span class="mi">0</span> <span class="k">if</span> <span class="n">multiples</span><span class="p">.</span><span class="nf">include?</span><span class="p">(</span><span class="mi">0</span><span class="p">)</span>

    <span class="p">(</span><span class="mi">0</span><span class="o">...</span><span class="n">limit</span><span class="p">).</span><span class="nf">sum</span> <span class="p">{</span> <span class="o">|</span><span class="n">integer</span><span class="o">|</span> <span class="n">factors_of</span><span class="p">(</span><span class="n">integer</span><span class="p">)</span> <span class="p">}</span>
  <span class="k">end</span>
<span class="k">end</span>
</code></pre></div></div>

<p>I’ve added the monkey patch as well.</p>

<p>Another enhancement is to change the <code class="language-plaintext highlighter-rouge">factor_of?</code> to <code class="language-plaintext highlighter-rouge">factors_of</code>, because it is what it does. It returns an array of factors. No more no less.</p>

<p>It was very pleasing to transform this kata code to a production code.</p>

<h3 id="conclusion">Conclusion</h3>

<p>Oneliners are great, as I said earlier, you can brag about, but no one cares. It’s not meant to be runned in production. If you think you can, you’ve been fooled. Runtimes or compilers doesn’t care about 1 or 10 lines.</p>

<p>But humans cares. They will read the code and, I’m sorry to say that the human brain doesn’t work like compiler.</p>

<p>Expressiveness is key, readibility is key and following SOLID principles is always a good think to do.</p>

:ET