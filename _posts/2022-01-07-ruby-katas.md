---
layout: post
title:  Some Ruby katas - Part 1
tags: ruby kata
category: kata
excerpt_separator:  <!--more-->
---

As I did the `rot13` in Elixir, trying to achieve it in Ruby would be fun too.
So in Ruby, you can get the codepoints of a `String` using `String#each_codepoint` and then call `to_a` like this.

```ruby
"abc".each_codepoint.to_a # => [97, 98, 99]
```
Fortunately, Ruby provides a shorthand way of doing this: `String#codepoints`.

<!--more-->

Since I need to generate an array of codepoints, mapping through the newly generated array of codepoints seems to be the next step.

```ruby
def rot13(string)
  string.codepoints.map {|c| p c}
end
```

Now it's time to use this `c` (which stands for codepoint). 
I need to check if `c` is included inside a range of codepoints.
I'll use same trick as in the Elixir solution, adding 13 if `c` is in the first half of the alphabet, or subtracting if it's in the other.

How to generate an array of codepoints?

```ruby
[*?a..?m].join.codepoints
# => [97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109]
```

But, I'd like to use a `case`and range comparison.
Nothing can't stop me using `97..109` in my solution. Let's do this.

```ruby
def rot13(string)
  string.codepoints.map {|c|
    case c
    when 97..109, 65..77 then c + 13
    when 110..122, 78..90 then c - 13
    else c        
    end
  }
end
```

Here, I'm mapping over the codepoints array.
Inside the block, I use a case and when `c` is inside one of these ranges, I add or subtract 13.

The fact is at the end, map give me back an array, with `"abc"` rot13 give me back `[110, 111, 112]`.

I need to somehow do the reverse of `String#codepoints`.

Well, `Array#pack` is here to help.

Pack expects a `templateString`. Reading the docs I can pass:

| Directive        | Element | Meaning                          |
|------------------|---------|----------------------------------|
| C                | Integer | 8-bit unsigned (unsigned char)   |
| U                | Integer | UTF-8 character                  |


```ruby
def rot13(string)
  string.codepoints.map {|c|
    case c
    when 97..109, 65..77 then c + 13
    when 110..122, 78..90 then c - 13
    else c        
    end
  }.pack('U')
end
```

Should do the job isn't it?

Nope...

We should pass a quantifier, like *.

```ruby
def rot13(string)
  string.codepoints.map {|c|
    case c
    when 97..109, 65..77 then c + 13
    when 110..122, 78..90 then c - 13
    else c        
    end
  }.pack('U*')
end
```

That's it!

Until the next one 👋👋