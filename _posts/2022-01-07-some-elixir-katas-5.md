---
layout: post
title:  Some Elixir katas - Part 5
tags: elixir kata
category: kata
excerpt_separator:  <!--more-->
---

I've lately decided to make some katas again.

Still on codewars' 5kyu tier, I've tackled the `rot13` exercice.

What's the gist of this one?

> ROT13 is a simple letter substitution cipher that replaces a letter with the letter 13 letters after it in the alphabet. ROT13 is an example of the Caesar cipher.

> Create a function that takes a string and returns the string ciphered with Rot13. If there are numbers or special characters included in the string, they should be returned as they are. Only letters from the latin/english alphabet should be shifted, like in the original Rot13 "implementation".

We'll need to handle strings, nice.

Based on the description, `test` should output `grfg`.

<!--more-->

```elixir
defmodule Encryptor do
  def rot13(string) do
    # do the stuff plz
  end
end

Encryptor.rot13("test") # => `grfg`
```

In Elixir, strings are lists. As shown on image below.

![String in elixir](/assets/img/elixir-string-list.png)

Well in fact, it's just `iex` helping us and displaying the `[97, 98, 99]` into a charlist of 'abc'.
In order to make it a `real` string, we can use `Kernel.to_string/1`. 
Since `Kernel` is already loaded, no need to call it explicitly.

And so for the opposite, `String.to_charlist/1`.
```
iex> to_string [97,98,99] # => "abc"
iex> String.to_charlist("test") # => 'test'
```

One thing that we could do is use `?` in front of any character literal to get its code point. 

```
iex> ?a # => 97
```

With this, we can confidently find a way to make this `rot13` a thing.

The alphabet is made up of 26 letters. We can check if the current `char` is in the first half or the second.
We can do this, this way:

```elixir
rot13 = fn(c) when ?a <= c and c <= ?m -> # do the stuff end
```

Recently I've discovered how powerful multiclause lambda can be. So I've decided to tackle this exercise using one.

We also need to covert the case where the letter is a capital.

```elixir
fn(c) when (?A <= c and c <= ?M) or (?a <= c and c <= ?m) end
```

Since we want to `rot13`, simply add 13 to the code point will make the job.

```elixir
fn(c) when (?A <= c and c <= ?M) or (?a <= c and c <= ?m) -> c + 13 end
```

And for the second half, well  we should subtract 13.

```elixir
fn(c) when (?A <= c and c <= ?M) or (?a <= c and c <= ?m) -> c + 13
  (c) when (?N <= c and c <= ?Z) or (?n <= c and c <= ?z) -> c - 13
end
```

And also when `c` does not fall inside the `guards`, simply put `c` back.

```elixir
fn(c) when (?A <= c and c <= ?M) or (?a <= c and c <= ?m) -> c + 13
  (c) when (?N <= c and c <= ?Z) or (?n <= c and c <= ?z) -> c - 13
  (c) -> c
end
```

This function takes a `char` as a parameter. Passing a plain string will produce an error.
As we've seen previously, ``String.to_charlist/1``will transform `"test"` into `'test'`. That's fine.

Then we can map and apply our function, and use `Kernel.to_string/1` or just `to_string` to produce a valid string which is expected.

```elixir
defmodule Encryptor do
  def rot13(string) do
    f = fn(c) when (?A <= c and c <= ?M) or (?a <= c and c <= ?m) -> c + 13
          (c) when (?N <= c and c <= ?Z) or (?n <= c and c <= ?z) -> c - 13
          (c) -> c
        end
    String.to_charlist(string) |> Enum.map(f) |> to_string
  end
end
```

The tricky part here, is to think that you can add/subtract 13 to achieve this goal.
Making usage of code points helps so much. We've tackled this exercise in a very declarative fashion, Elixir is well built for that. But I've seen solutions that are very imperative and it ask the programmer to fight against the language. Try to always follow the idioms of the language. The code will be clearer, less confuse and error prone.

Speaking of idioms...

```elixir
defmodule Encryptor do
  def rot13(string) do    
    f = fn(c) when c in ?A..?M or c in ?a..?m -> c + 13
          (c) when c in ?N..?Z or c in ?n..?z -> c - 13
          (c) -> c
        end
        
    String.to_charlist(string) |> Enum.map(f) |> to_string
  end
end
```

One enhancement is to, rather than imperatively check for bounds, just ask if `c` is inside a given range.
It's almost like plain English.

Until the next one 👋👋
